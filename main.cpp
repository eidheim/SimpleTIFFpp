#include <iostream>
#include "tiff.hpp"

using namespace std;
using namespace SimpleTIFFpp;

int main() {
    try {
        //Example 1:
        //Loading a tiff into vector<unsigned char> (white=255, black=0)
        //Writing vector<unsigned char> to a new TIFF-file
        Tiff<unsigned char> tiff_gray;
        auto image_gray=tiff_gray.read("gray.tiff");
        tiff_gray.write("gray_copy.tiff", image_gray);
        
        //Example 2:
        //Loading a tiff into vector<double> (white=1.0, black=0.0)
        //Writing vector<double> to a new TIFF-file
        Tiff<double> tiff_gray_double;
        auto image_gray_double=tiff_gray_double.read("gray.tiff");
        tiff_gray_double.write("gray_copy_2.tiff", image_gray_double);
        
        //Example 3:
        //Creating a new vector<double> volume (2x2x2) and storing it as a new TIFF-file
        vector<float> new_gray_image(8);
        //z==0:
        new_gray_image[0]=0.0;
        new_gray_image[1]=1.0;
        new_gray_image[2]=1.0;
        new_gray_image[3]=0.5;
        //z==1:
        new_gray_image[4]=1.0;
        new_gray_image[5]=0.0;
        new_gray_image[6]=0.0;
        new_gray_image[7]=0.5;

        Tiff<float>::write("new_gray.tiff", new_gray_image, 2, 2);
        
        //Example 4:
        //Reading a RGB- or RGBA-TIFF file
        //Outputs image size
        //Setting all white pixels/voxels as transparent
        //Writing vector<RGBA> to a new TIFF-file
        Tiff<RGBA> tiff_rgba;
        auto image_rgba=tiff_rgba.read("rgb_or_rgba.tiff");
        cout << "size of image rgb_or_rgba.tiff: " << tiff_rgba.size_x << "x" << tiff_rgba.size_y << "x" << tiff_rgba.size_z << endl;
        for(auto& rgb: image_rgba) {
            if(rgb.r==255 && rgb.g==255 && rgb.b==255) {
                rgb.a=0;
            }
        }

        tiff_rgba.write("rgba_transparent.tiff", image_rgba);
    }
    catch(exception& e) {
        cerr << e.what() << endl;
    }
    
    return 0;
}

