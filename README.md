SimpleTIFFpp
============

A very easy to use C++11 TIFF-reader and writer using LibTIFF. Supports uncompressed and simple gray and RGB(A) images and volumes. 

Tested with images and volumes created with GIMP and MATLAB

###Usage

See main.cpp for example usage. 

### Dependency

LibTIFF must be installed (http://www.remotesensing.org/libtiff/)

### Compile and run

Compile with a C++11 compiler:

g++ -std=c++11 -O3 -ltiff main.cpp -o examples

Then run: ./examples

