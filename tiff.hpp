#ifndef TIFF_HPP
#define	TIFF_HPP

#include<tiffio.h>

#include<vector>
#include<math.h>
#include<string>
#include<stdexcept>

namespace SimpleTIFFpp {
    struct RGB {
        unsigned char r;
        unsigned char g;
        unsigned char b;
    };

    struct RGBA {
        unsigned char r;
        unsigned char g;
        unsigned char b;
        unsigned char a;
    };

    template <class type>
    class Tiff {
    public:
        uint32 size_x, size_y, size_z;
        
        std::vector<type> read(const std::string& filename) {
            TIFF *tiff_in = TIFFOpen(filename.c_str(), "r");
            if (!tiff_in)
                throw std::invalid_argument("Couldn't open file: " + filename);

            TIFFGetField(tiff_in, TIFFTAG_IMAGEWIDTH, &size_x);
            TIFFGetField(tiff_in, TIFFTAG_IMAGELENGTH, &size_y);

            uint16 spp;
            TIFFGetField(tiff_in, TIFFTAG_SAMPLESPERPIXEL, &spp);
            
            size_z = 0;
            do {
                size_z++;
            } while (TIFFReadDirectory(tiff_in));
            TIFFSetDirectory(tiff_in, 0);

            std::vector<type> result(size_x * size_y * size_z);

            unsigned char* row = (unsigned char*) _TIFFmalloc(TIFFScanlineSize(tiff_in));
            for (size_t z = 0; z < size_z; z++) {
                for (size_t y = 0; y < size_y; y++) {
                    TIFFReadScanline(tiff_in, row, y);
                    for (size_t x = 0; x < size_x; x++) {
                        result[z * size_x * size_y + y * size_x + x] = get_pixel(row, x, spp);
                    }
                }
                TIFFReadDirectory(tiff_in);
            }
            _TIFFfree(row);
            TIFFClose(tiff_in);

            return result;
        }
        
        void write(const std::string& filename, const std::vector<type>& data) {
            Tiff<type>::write(filename, data, size_x, size_y);
        }
        
        static void write(const std::string& filename, const std::vector<type>& data, uint32 size_x, uint32 size_y) {
            uint32 size_z = data.size() / (size_x * size_y);

            TIFF *tiff_out = TIFFOpen(filename.c_str(), "w");
            if (!tiff_out)
                throw std::invalid_argument("Couldn't save file: " + filename);

            uint16 spp = Tiff<type>::get_spp();

            unsigned char *row = (unsigned char*) malloc(sizeof (unsigned char)*size_x * spp);

            for (size_t z = 0; z < size_z; z++) {
                TIFFSetField(tiff_out, TIFFTAG_IMAGEWIDTH, size_x);
                TIFFSetField(tiff_out, TIFFTAG_IMAGELENGTH, size_y);
                TIFFSetField(tiff_out, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
                TIFFSetField(tiff_out, TIFFTAG_SAMPLESPERPIXEL, spp);
                TIFFSetField(tiff_out, TIFFTAG_BITSPERSAMPLE, 8);
                TIFFSetField(tiff_out, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
                TIFFSetField(tiff_out, TIFFTAG_PHOTOMETRIC, Tiff<type>::get_photometric());
                TIFFSetField(tiff_out, TIFFTAG_COMPRESSION, COMPRESSION_NONE);

                TIFFSetField(tiff_out, TIFFTAG_ROWSPERSTRIP, 1);

                TIFFSetField(tiff_out, TIFFTAG_XRESOLUTION, 1.0);
                TIFFSetField(tiff_out, TIFFTAG_YRESOLUTION, 1.0);
                TIFFSetField(tiff_out, TIFFTAG_RESOLUTIONUNIT, 1);

                TIFFSetField(tiff_out, TIFFTAG_PAGENUMBER, z, size_z);

                for (size_t y = 0; y < size_y; y++) {
                    for (size_t x = 0; x < size_x; x++) {
                        Tiff<type>::set_pixel(row, data[z * size_x * size_y + y * size_x + x], x, spp);
                    }
                    TIFFWriteScanline(tiff_out, row, y, 0);
                }

                TIFFWriteDirectory(tiff_out);
            }
            
            if (row)
                free(row);
            TIFFClose(tiff_out);
        }
    private:
        friend class Tiff<float>;
        friend class Tiff<double>;
        
        static uint16 get_spp() {
            return 1;
        }
        
        static uint16 get_photometric() {
            return 1; //1=Gray, 2=RGB(A)
        }
        
        static type get_pixel(unsigned char* row, size_t x, uint16 spp) {
            unsigned char v=0;
            if (spp == 1)
                v=*(row + x);
            else if (spp == 2)
                v=*(row + x * spp);
            else if (spp >= 3) {
                uint32* color = (uint32*) (row + (x * spp));
                v=(TIFFGetR(*color) + TIFFGetG(*color) + TIFFGetB(*color)) / 3;
            }
            return v;
        }
        
        static void set_pixel(unsigned char* row, const type& pixel, size_t x, uint16 spp) {
            row[x] = pixel;
        }
    };
    
    template<>
    uint16 Tiff<RGBA>::get_spp() {
        return 4;
    }
    
    template<>
    uint16 Tiff<RGB>::get_spp() {
        return 3;
    }
    
    template <>
    uint16 Tiff<RGBA>::get_photometric() {
        return 2; //1=Gray, 2=RGB(A)
    }
    
    template <>
    uint16 Tiff<RGB>::get_photometric() {
        return 2; //1=Gray, 2=RGB(A)
    }
    
    template<>
    float Tiff<float>::get_pixel(unsigned char* row, size_t x, uint16 spp) {
        return float(Tiff<unsigned char>::get_pixel(row, x, spp))/255.0;
    }
    
    template<>
    double Tiff<double>::get_pixel(unsigned char* row, size_t x, uint16 spp) {
        return double(Tiff<unsigned char>::get_pixel(row, x, spp))/255.0;
    }

    template<>
    RGBA Tiff<RGBA>::get_pixel(unsigned char* row, size_t x, uint16 spp) {
        RGBA rgba;
        
        unsigned char v;
        if (spp == 1) {
            v = *(row + x);
            rgba.r = v;
            rgba.g = v;
            rgba.b = v;
            rgba.a = 255;
        } else if (spp == 2) {
            v = *(row + x * spp);
            rgba.r = v;
            rgba.g = v;
            rgba.b = v;
            rgba.a = 255;
        } else if (spp == 3) {
            uint32* color = (uint32*) (row + (x * spp));
            rgba.r = TIFFGetR(*color);
            rgba.g = TIFFGetG(*color);
            rgba.b = TIFFGetB(*color);
            rgba.a = 255;
        } else if (spp >= 4) {
            uint32* color = (uint32*) (row + (x * spp));
            rgba.r = TIFFGetR(*color);
            rgba.g = TIFFGetG(*color);
            rgba.b = TIFFGetB(*color);
            rgba.a = TIFFGetA(*color);
        }
        
        return rgba;
    }
    
    template<>
    RGB Tiff<RGB>::get_pixel(unsigned char* row, size_t x, uint16 spp) {
        RGB rgb;
        
        unsigned char v=0;
        if (spp == 1) {
            v = *(row + x);
            rgb.r = v;
            rgb.g = v;
            rgb.b = v;
        } else if (spp == 2) {
            v = *(row + x * spp);
            rgb.r = v;
            rgb.g = v;
            rgb.b = v;
        } else if (spp >= 3) {
            uint32* color = (uint32*) (row + (x * spp));
            rgb.r = TIFFGetR(*color);
            rgb.g = TIFFGetG(*color);
            rgb.b = TIFFGetB(*color);
        }
        
        return rgb;
    }
    
    template<>
    void Tiff<float>::set_pixel(unsigned char* row, const float& pixel, size_t x, uint16 spp) {
            row[x] = round(pixel*255.0);
    }
    
    template<>
    void Tiff<double>::set_pixel(unsigned char* row, const double& pixel, size_t x, uint16 spp) {
            row[x] = round(pixel*255.0);
    }
    
    template<>
    void Tiff<RGBA>::set_pixel(unsigned char* row, const RGBA& pixel, size_t x, uint16 spp) {
        row[x * spp] = pixel.r;
        row[x * spp + 1] = pixel.g;
        row[x * spp + 2] = pixel.b;
        row[x * spp + 3] = pixel.a;
    }
    
    template<>
    void Tiff<RGB>::set_pixel(unsigned char* row, const RGB& pixel, size_t x, uint16 spp) {
        row[x * spp] = pixel.r;
        row[x * spp + 1] = pixel.g;
        row[x * spp + 2] = pixel.b;
    }
}

#endif	/* TIFF_HPP */